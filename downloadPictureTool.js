var http = require('http');
var fs = require('fs');
var Parse = require('parse/node');
var async = require("async");
var path = require('path');

// Application ID and Javascript Key
Parse.initialize("mslr6chIzlzflTF1LzmSXCkqsPZJdvu8eJ3NIYpW", "909zfJLSkcs3GNFHuHVIDbISfocpGURyPT3G4OJ2");

var Media_Children = new Parse.Object.extend('Media_Children');
//var MediaStore = new Parse.Object.extend('MediaStore');
var results = [];
var limitNumber = 1000;
var timeout = 300;

//root directory
var dir = 'MediaStore';
//directory array, record all directory we create
var array = [];

var start = function(record_id){
  fs.readdir('MediaStore', function(err, list) {
    if (err){
      //create the root directory
      fs.mkdir(dir);
      process(false);
    } else {
      var pending = list.length;
      list.forEach(function(file) {
        array.push(dir + '/' + file);
      });
      process(record_id);
    }
  });
}

//get limited number of records
var process = function(skip){
  console.log('loading data...');
  var query = new Parse.Query(Media_Children);
  if (skip) {
    query.greaterThan('objectId', skip);
  }
  query.limit(limitNumber);
  query.ascending('objectId');
  query.include('child');
  query.include('media');
  query.find({
    success: function(response) {
      processCallback(response);
    },
    error: function(error){
      response.error(error);
    }
  });
}

var processCallback = function(response){
  results.push.apply(results, response);
  if (response.length == 1000) {
    process(response[response.length - 1].id);
  } else {
    console.log('data loaded.');
    //now we get all records

    //this function will porcess results one by one
    async.eachSeries(results, function(result, callback) {
      console.log(result.id);
      if (typeof result.get('media') !== 'undefined' && typeof result.get('child') !== 'undefined') {
        setTimeout(function(){
          //var query2 = new Parse.Query(MediaStore);
          var prefix = result.get('media').id + '_' +result.get('media').createdAt;
          var childDir = dir + '/' + result.get('child').get('firstName') + ' ' +
          result.get('child').get('lastName') + ' ' + result.get('child').id;

          if (array.indexOf(childDir) <= -1) {
            fs.mkdir(childDir);
            array.push(childDir);
          }

          var media = result.get('media');

          if (media.get('mediaType') == 'reportEYLF') {
            download(media.get('thumbFile'),prefix, childDir, function(result){
              console.log(result);
              callback();
            });
          } else {
            download(media.get('mediaFile'),prefix, childDir, function(result){
              console.log(result);
              callback();
            });
          }
        },timeout);
      }else {
        callback();
      }
    }, function(err){
        if( err ) {
          console.log('ERROR HAPPENS!!!');
          console.log(err);
        } else {
          console.log('All files have been processed successfully');
        }
    });
  }
}

var download = function(result, prefix, dir, cb) {
  console.log(result._name + ' downloading...');
  var file = fs.createWriteStream(dir + '/' + prefix + '.' + result._name.split('.')[1]);
  var request = http.get(result._url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb(result._name + ' finish.'));  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    if (cb) cb(err.message);
  });
};

//you can pass the record id here
start(false);

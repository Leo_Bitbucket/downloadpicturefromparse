var fs = require('fs');
var path = require('path');
var pdf = require('html-pdf');
var swig  = require('swig');
var path = require('path');

var map = fs.readFileSync('data.json', 'utf8');
var jsonData = JSON.parse(map);

//this function will get all files from this dir
var walk = function(dir, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    var pending = list.length;
    if (!pending) return done(null, results);
    list.forEach(function(file) {
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        } else {
          results.push(file);
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

walk('MediaStore', function(err, results) {
  //now we get all files in this directory
  for (var i = 0; i < results.length; i++) {
    if (path.extname(results[i]) == '.json') {
      // Get content from json file
      var contents = fs.readFileSync(results[i], 'utf8');
      var jsonContent = JSON.parse(contents);
      var directory = path.dirname(results[i]);

      var imageUrls = [];
      for (var j = 0; j < jsonContent.mediaArray.length; j++) {
        var imageId = jsonContent.mediaArray[j];
        imageUrls.push(jsonData[imageId]);
      }

      var paragraphs = [];
      for (var k = 0; k < jsonContent.paragraphArray.length; k++) {
        var paragraph = jsonContent.paragraphArray[k];
        paragraphs.push(paragraph);
      }

      //render html file
      var template = swig.compileFile('template.html');
      var output = template({
          content: jsonContent,
          imageUrls: imageUrls,
          paragraphs: paragraphs
      });

      //html to pdf
      var options = { format: 'Letter',
                      header: {"height": "20mm"},
                      footer: {"height": "10mm"}
                    };
      pdf.create(output, options).toFile(directory + '/' + path.basename(results[i]).slice(0,-5) + '.pdf', function(err, res) {
        if (err)
          return console.log(err);
        console.log(res);
      });
    }
  }
});

var http = require('http');
var fs = require('fs');
var Parse = require('parse/node');
var async = require("async");

// Application ID and Javascript Key
Parse.initialize("mslr6chIzlzflTF1LzmSXCkqsPZJdvu8eJ3NIYpW", "909zfJLSkcs3GNFHuHVIDbISfocpGURyPT3G4OJ2");

var MediaStore = new Parse.Object.extend('MediaStore');
var results = [];
var limitNumber = 1000;

//get limited number of records
var process = function(skip){
  console.log('loading data...');
  var query = new Parse.Query(MediaStore);
  if (skip) {
    query.greaterThan('objectId', skip);
  }
  query.limit(limitNumber);
  query.ascending('objectId');
  query.find({
    success: function(response) {
      processCallback(response);
    },
    error: function(error){
      response.error(error);
    }
  });
}

var processCallback = function(response){
  results.push.apply(results, response);
  if (response.length == 1000) {
    process(response[response.length - 1].id);
  } else {
    console.log('data loaded.');
    //now we get all records
    //record key.value: keyFromDevice we file url
    var map = new Object();

    async.each(results, function(result, callback) {
      console.log(result.get('keyFromDevice'));
      if (typeof result.get('keyFromDevice') !== 'undefined') {
          //save every keyFromDevice and file url pair to the obect
          if (result.get('mediaType') == 'video') {
            map[result.get('keyFromDevice')] = result.get('thumbFile')._url;
          }else {
            map[result.get('keyFromDevice')] = result.get('mediaFile')._url;
          }
          callback();
      }else {
        callback();
      }
    }, function(err){
        if( err ) {
          console.log('ERROR HAPPENS!!!');
          console.log(err);
        } else {
          fs.writeFileSync('data.json', JSON.stringify(map), 'utf-8');
          console.log('All files have been processed successfully');
        }
    });
  }
}

process(false);
